<?php
require_once '../vendor/PhilKershaw/Maturity.php';

class Maturity_Test extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->maturity = new \PhilKershaw\Maturity('../MaturityData.csv');
		$this->policy_no = 'A100001';
		$this->maturity->setData(
			array(
				array(
	            	'policy_number' => 'A100003',
	            	'policy_start_date' => '31/12/1989',
	            	'premiums' => 15250,
	            	'membership' => 'N',
	            	'discretionary_bonus' => 1600,
	            	'uplift_percentage' => 42
	        	)
			)
		);
	}

	public function testGetPolicyType()
	{
		$type = $this->maturity->getPolicyType($this->policy_no);
		$this->assertEquals('A', $type);
	}

	public function testBonusQualification()
	{
		// testing class 'A' policies
		$type = 'A';
		$start_date = '01/06/1986';
		$membership = 'Y';
		$qualified = $this->maturity->bonusQualification($type, $start_date, $membership);
		$this->assertTrue($qualified);

		$start_date = '01/01/1990';
		$qualified = $this->maturity->bonusQualification($type, $start_date, $membership);
		$this->assertFalse($qualified);

		// testing class 'B' policies
		$type = 'B';
		$qualified = $this->maturity->bonusQualification($type, $start_date, $membership);
		$this->assertTrue($qualified);

		$membership = 'N';
		$qualified = $this->maturity->bonusQualification($type, $start_date, $membership);
		$this->assertFalse($qualified);

		// testing class 'C' policies
		$type = 'C';
		$start_date = '01/06/1986';
		$membership = 'Y';
		$qualified = $this->maturity->bonusQualification($type, $start_date, $membership);
		$this->assertFalse($qualified);

		$membership = 'N';
		$start_date = '01/06/1996';
		$qualified = $this->maturity->bonusQualification($type, $start_date, $membership);
		$this->assertFalse($qualified);

		$membership = 'Y';
		$start_date = '01/06/1996';
		$qualified = $this->maturity->bonusQualification($type, $start_date, $membership);
		$this->assertTrue($qualified);
	}

	public function testGetManagementFee()
	{
		$type = 'A';
		$fee = $this->maturity->getManagementFee($type);
		$this->assertEquals(3, $fee);

		$type = 'B';
		$fee = $this->maturity->getManagementFee($type);
		$this->assertEquals(5, $fee);

		$type = 'C';
		$fee = $this->maturity->getManagementFee($type);
		$this->assertEquals(7, $fee);
	}

	public function testGetMaturityValue()
	{
		$maturity = $this->maturity->getMaturityValue(0);
		$this->assertArrayHasKey('policy_number', $maturity);
		$this->assertArrayHasKey('maturity_value', $maturity);
		$this->assertEquals(23277.35, $maturity['maturity_value']);
	}
}