<?php
if (PHP_SAPI === 'cli'):
	require_once 'vendor/PhilKershaw/Maturity.php';
	$args     = array_slice($argv, 1);
	try {
		if(!$args)
			throw new Exception("
Please provide a CSV file path and XML filename (to save to).\n
Format:\n
php calc_maturity.php [CSV file path] [XML filename]
			");
		$csv          = $args[0];
		$xml_filename = $args[1];
	} catch (Exception $e) {
		echo $e->getMessage() . "\n\n";
		exit;
	}
	$maturity = new \PhilKershaw\Maturity($csv);
	$data = $maturity->getData();
	$xml_data = array();
	foreach($data as $id => $row)
	{
		$value = $maturity->getMaturityValue($id);
		$xml_data[$value['policy_number']] = array(
			$value['maturity_value'] => $value['policy_number']
		);
	}
	$xml = new SimpleXMLElement('<maturity/>');
	array_walk_recursive($xml_data, array($xml, 'addChild'));
	if($xml->asXML($xml_filename))
		echo "Created file '{$xml_filename}' successfully.\n";
endif;
?>