<?php
/**
 * Maturity
 *
 * A simple utility for calculating the value of maturing insurance policies.
 *
 * @package		PhilKershaw
 * @version		1.0
 * @author		Phil Kershaw
 * @license		N/A
 * @link		http://blog.philkershaw.me
 */
namespace PhilKershaw;
use Exception;

class Maturity
{
	/**
	 * Base Date
	 * 
	 * @var string
	 */
	public $base_date = '01/01/1990';
	/**
	 * Data
	 * 
	 * @var array
	 */
	protected $data = array();
	/**
	 * Parse CSV
	 * 
	 * Parses the CSV file and returns the data array unless $return 
	 * is set to false, then returns true on success or an error on failure.
	 * 
	 * @param string
	 * @param bool
	 * @return array
	 */
	public function __construct($csv_file = null)
	{
		try {
			if(!is_readable($csv_file))
				throw new Exception('CSV file not found. Please check the path and try again.');
			$csv = fopen($csv_file, 'r');
			$headers = fgetcsv($csv);
			while(!feof($csv))
			{
				$data = fgetcsv($csv);
				if($data)
					$this->data[] = array_combine($headers, $data);
			}
			fclose($csv);
		} catch (Exception $e) {
			echo $e->getMessage() . "\n\n";
			exit;
		}
	}
	/**
	 * Get Data
	 * 
	 * Returns the parsed CSV data array.
	 * 
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}
	/**
	 * Set Data
	 * 
	 * Allows the setting of the data variable - mainly used for testing.
	 * 
	 * @param array
	 */
	public function setData($data)
	{
		if(is_array($data))
			$this->data = $data;
	}
	/**
	 * Get Policy Type
	 * Extracts the policy type from the policy number.
	 * 
	 * @param string
	 * @return string
	 */
	public function getPolicyType($policy_no)
	{
		try {
			$type = substr($policy_no, 0, 1);
			$possible_types = array('A','B','C');
			if (!in_array($type, $possible_types))
			{
				throw new Exception('Invalid policy number.');
			}
			return $type;
		} catch (Exception $e) {
			echo $e->getMessage() . "\n\n";
			exit;
		}
	}
	/**
	 * Bonus Qualification
	 * Checks if the policy qualifies for a discretionary bonus.
	 * 
	 * @param string
	 * @param date
	 * @param bool
	 * @return bool
	 */
	public function bonusQualification($type, $start_date, $membership)
	{
		$base_date = strtotime($this->base_date);
		$start_date = strtotime($start_date);
		switch($type)
		{
			case 'A':
				if($start_date < $base_date)
					return true;
				return false;
				break;
			case 'B':
				if($membership == 'Y')
					return true;
				return false;
				break;
			case 'C':
				if($start_date >= $base_date AND $membership == 'Y')
					return true;
				return false;
				break;
		}
		return false;
	}
	/**
	 * Get Management Fee
	 * Returns the management fee for the policy type passed.
	 * 
	 * @param string
	 * @return integer
	 */
	public function getManagementFee($type)
	{
		$fees = array(
			'A' => 3,
			'B' => 5,
			'C' => 7
		);
		return $fees[$type];
	}
	/**
	 * Calculate Maturity Value
	 * Calculates the maturity value.
	 * 
	 * @param integer
	 * @param integer
	 * @param integer
	 * @param mixed
	 * @return integer
	 */
	public function getMaturityValue($id)
	{
		$number     = $this->data[$id]['policy_number'];
		$start_date = $this->data[$id]['policy_start_date'];
		$membership = $this->data[$id]['membership'];
		$premiums   = $this->data[$id]['premiums'];
		$uplift     = $this->data[$id]['uplift_percentage'];
		$type       = $this->getPolicyType($number);
		$qualify    = $this->bonusQualification($type, $start_date, $membership);
		$bonus      = ($qualify) ? $this->data[$id]['discretionary_bonus'] : 0;
		$fee        = $this->getManagementFee($type);

		$management_fee = $premiums * $fee / 100;
		$uplift = ($uplift / 100) + 1;
		$value = ((($premiums - $management_fee) + $bonus) * $uplift);

		return array(
			'policy_number' => $number,
			'maturity_value' => $value
		);
	}
}