<?php
// Class autoloader
function autoloader($class)
{
    $class = dirname(__FILE__) . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
    if(is_readable($class))
    {
        require_once($class);
    }
}
spl_autoload_register('autoloader');