#Maturity Calculator

A simple application to value maturing insurance policies. Utilises a simple class utility to aid the calculation.

##Usage

	php calc_maturity.php [CSV file path] [Desired XML filename (incl extension)]
